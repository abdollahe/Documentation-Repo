# ROS controller Documentation


## controller type 'effort_controllers/JointPositionController' does not exist


For controller errors in spawing robots in Gazebo and ROS be sure to have the 
contoroller related packages installed for the specific ROS distro. For example if the ROS distro is noetic then the following commands need to be executed:

1- `sudo apt-get install ros-noetic-controller-manager`
2- `sudo apt-get install ros-noetic-ros-control`
3- `sudo apt-get install ros-noetic-ros-controllers`

Then after running the above commands to install the controller packages we can check to see if a controller type is installed or not by the following command:
 For example in  this command we are trying to look for the effort_controller installation path:

`rospack find effort_controllers`

its response is the follwing:

`/opt/ros/noetic/share/effort_controllers`




