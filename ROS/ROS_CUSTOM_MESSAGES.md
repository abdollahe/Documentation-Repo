# In the name of Allah
This document contains information on how to create and use ROS messages in C++.
---

## How to create a Custom Message type in ROS

1. Create a package
2. In the package directory create a msg folder
3. Create a text file with the desired name and the save it as a .msg file.
   Example:
   `
   string first_name
   string last_name
   uint8 age
   
   `
4. Add the following to the packages package.xml file:

    - <build_depend>message_generation</build_depend> 
    - <exec_depend>message_runtime</exec_depend>
   
5. Add the following to the packages CMakeLists.txt file:
    - Add the phrase "message_generation" to the find_package function.
        `
        find_package(catkin REQUIRED COMPONENTS
                roscpp
                rospy
                std_msgs
                message_generation )
            
        `
    - Make sure to export the message runtime dependency by adding the phrase "message_runtime" to the catkin_package function after the phrase "CATKIN_DEPENDS"
        `
        catkin_package(
            ...
            CATKIN_DEPENDS message_runtime ...
        ...)
        
        `
    - Add the message files name to the add_message_files function:
        `
        add_message_files(
            FILES
            example.msg
            )
                    
        `
    - Ensure that the generate_messages function is called:
            `
            generate_messages(
                DEPENDENCIES 
                std_msgs)
            `
## How to use the message

1. In the same package:
   Simply add the generated header bu the following syntax in the desired source file:
   ` #include <your_package_name/Example.h> `
2. In another package:
   
   - Add the following two lines to the package.xml file in the package you want to use the message:
        - ` <build_depend>package_name_containing_the_custom_message</build_depend> `
        - ` <exec_depend>package_name_containing_the_custom_message</exec_depend> `
   - Add the name of the package to the "find_package" function of the CMakeLists.txt file of the package that is to use the message.
        - ` find_package(catkin REQUIRED COMPONENTS roscpp std_msgs name_of_pckage_containing_message) `         
                    
   - Add the following for the target executables or libraries to be generated:
        - ` add_dependencies(your_program ${catkin_EXPORTED_TARGETS}) `
        - ` add_dependencies(your_library ${catkin_EXPORTED_TARGETS}) `
                             

